# Unreachable

Have you concluded with perfect certainty that a part of your code may not, will not, *could not* logically be reached and you still feel anxious? Use this exception to cover all bases. When the laws of logic cease, your program will take note of it.

```python
from unreachable import Unreachable
```
